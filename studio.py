#!/usr/bin/python3
#-----------------------------------------------------------
#    ___  ___  _ ____
#   / _ \/ _ \(_) __/__  __ __
#  / , _/ ___/ /\ \/ _ \/ // /
# /_/|_/_/  /_/___/ .__/\_, /
#                /_/   /___/
#
# Project : Stop Motion Animation Studio
# File    : studio.py
#
# Script to create Flask based website.
#
# Author : Matt Hawkins
# Date   : 19/02/2019
# Source : https://bitbucket.org/MattHawkinsUK/rpispy-animation-studio/
#
# Additional details here:
# https://www.raspberrypi-spy.co.uk/
#
#-----------------------------------------------------------

import os
import getpass

from time import sleep

import config as c
import functions as f

from flask import Flask, flash, render_template, redirect, url_for

app = Flask(__name__)

@app.route("/")
def index():
  # Get list of sequence directories
  seqdirs=[]
  for entry in os.listdir(c.IMGDIR):
    if os.path.isdir(os.path.join(c.IMGDIR,entry)) == True:
        seqdirs.append(entry)

  seqdirs.sort(reverse=True)

  return render_template('index.html', dirs=seqdirs)

@app.route("/seq/<seqname>")
def seqGallery(seqname):

  seqpath=os.path.join(c.IMGDIR,seqname)

  frames=[]
  for entry in os.listdir(seqpath):
    if os.path.isfile(os.path.join(seqpath,entry)) == True:
      if entry[0:5]=='frame':
        frames.append(entry)

  frames.sort(reverse=True)

  return render_template('sequence.html', seqpath=seqpath, seq=seqname, frames=frames)

@app.route("/config")
def config():
  data=""
  with open('config.py', 'r') as myfile:
    while True:
      line = myfile.readline()
      if len(line)==1:
        break
    while line:
      line=myfile.readline()
      data=data+line

  data=data+"\n\n# Script User\n"+getpass.getuser()
  data=data.replace('\n', '<br />')

  return render_template('config.html', content=data)

@app.route("/delseq/<seq_dir>")
def delete_sequence(seq_dir):
  # Delete specified sequence
  f.deleteSeq(seq_dir)
  #flash("Deleted "+seq_dir)
  return redirect(url_for('index'))

@app.route("/delframe/<seq_dir>/<frame_name>")
def delete_frame(seq_dir,frame_name):
  # Delete specified image
  f.deleteFrame(seq_dir,frame_name)
  return redirect(url_for('seqGallery',seqname=seq_dir))

@app.route("/seqpreview/<seq_dir>")
def seq_preview(seq_dir):
  # Preview current frames
  preview_video=f.generatePreviewVideo(seq_dir)
  #preview_video='/images/'+seq_dir+'/preview.mp4'
  return render_template('preview.html', seq_dir=seq_dir, preview_video=preview_video, videosize=c.THUMBSIZE)

@app.route("/seqarchive/<seq_dir>")
def seq_archive(seq_dir):
  # Create archive
  archive_file=f.generateArchiveFile(seq_dir)
  return render_template('archive.html', seq_dir=seq_dir, archive_file=archive_file)

@app.route("/test")
def test():
  # Test
  #cam_capture=True
  pass
  return redirect(url_for('index'))
  
  
if __name__ == "__main__":
  app.run(host='0.0.0.0',debug=False)
