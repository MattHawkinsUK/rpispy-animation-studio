#-----------------------------------------------------------
#    ___  ___  _ ____
#   / _ \/ _ \(_) __/__  __ __
#  / , _/ ___/ /\ \/ _ \/ // /
# /_/|_/_/  /_/___/ .__/\_, /
#                /_/   /___/
#
# Project : Stop Motion Animation Studio
# File    : config.py
#
# This file contains configuration options
# that can be customised by the user.
#
# Author : Matt Hawkins
# Date   : 28/02/2019
# Source : https://bitbucket.org/MattHawkinsUK/rpispy-animation-studio/
#
# Additional details here:
# https://www.raspberrypi-spy.co.uk/
#
#-----------------------------------------------------------

# Name of this camera (max 10 chars)
CAMNAME='Camera 1'

# Prefix used for image files
PREFIX='cam1'

# Resolution of photos
#   Resolution  Aspect Framerates  Video Image FoV     Binning
#1  1920x1080   16:9   1-30fps     x           Partial None
#2  2592x1944   4:3    1-15fps     x     x     Full    None
#3  2592x1944   4:3    0.1666-1fps x     x     Full    None
#4  1296x972    4:3    1-42fps     x           Full    2x2
#5  1296x730    16:9   1-49fps     x           Full  2x2
#6  640x480     4:3    42.1-60fps  x           Full  4x4
#7  640x480     4:3    60.1-90fps  x           Full  4x4
PHOTOSIZE=2592,1944
PREVIEWSIZE=660,480

# Angle to rotate image
CAMROTATION=180

# Set preferred AWB mode to match lighting
CAM_AWB_MODE='sunlight'

# Directory where sequences will be saved
IMGDIR='images'

# GPIO used by LEDs
GPIOLEDGRN=16
GPIOLEDYEL=12

# GPIO used by light relay
GPIOLIGHT1=23
GPIOLIGHT2=24

# Size of attached I2C display
LCDW=128
LCDH=32

# Initial light state
LIGHTS_INIT=False

# Frame thumbnail size
THUMBSIZE=640,480

# Frame rate for preview videos
PREVIEWRATE=8

# curses text interface
TXTH=13
TXTW=41