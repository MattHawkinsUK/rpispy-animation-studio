# RPiSpy Animation Studio

Stop Motion Animation script for use with the Raspberry Pi camera. It creates a sequence of images which can be copied onto a PC for further editing.

## Hardware
The hardware used in this project includes :

*  Raspberry Pi
*  Raspberry Pi Camera
*  Power supply
*  SD card
*  128x32 OLED I2C display module
*  Push-to-make switch
*  LED with in-line resistor

The LED is connected to physical pins 30 and 32 (Ground and GPIO12).

The button is connected to physical pins 39 and 40 (Ground and GPIO21).

The I2C display module is connected to physical pins 1 (3.3V),3 (SDA),5 (SCL) and 6 (Ground).

## Software Setup
### Create Fresh SD Card
It is highly recommended that you create a new SD card using the latest version of Raspbian.

### Enable Pi Camera and I2C
The Pi camera must be enabled. Run the config utility:

    sudo raspi-config

From the available options :

*  Select "Interfacing options"
*  Select "Enable Pi Camera"
*  Select "Enable I2C"

### Download files
Ensure you are in the Pi home directory:

    cd ~

Clone the respository:

    git clone https://bitbucket.org/MattHawkinsUK/rpispy-animation-studio.git

Rename the directory to something shorter :

    mv rpispy-animation-studio.git animation

### Reboot
The Pi must be rebooted to ensure the Pi camera and I2C display are enabled.

    sudo reboot

## Basic Operation
To run the script start from the Pi user home directory :

    cd ~

Then navigate to the animation directory :

    cd animation

The run the Python script :

    python3 camera.py

When the script runs a new directory is created within the "images" directory. This is numbered based on other directories that may exist.

Pressing the button will capture a new image. This frame is numbered based on other frames that may exist in the sequence directory.

If enabled in the config file a thumbnail image will also be created for this frame.

## Thumbnail Images
If the thumbnails settting is set to True on the config file each frame will also have a thumbnail file created. The resolution of the thumbnail can be set in the config file.

These thumbnails can be used to create a quicker preview video.

## File structure
The script assumes there is a sub-folder named "images". Within that folder it creates a new folder for each sequence. This is prefixed with "cam1" and this can be changed in the config.py file.

Each sequence folder has a four digit number. Every time the script runs these 

```
animation
-- images
   -- cam1_seq0001
      -- frame_0001.jpg
	  -- thumb_frame_0001.jpg
	  -- frame_0002.jpg
	  -- thumb_frame_0002.jpg
	  ....
	  -- frame_####.jpg
	  -- thumb_frame_####.jpg
```

## Samba File Sharing
Enabling file sharing on the images directory makes it easy to copy image files onto a PC. Details TBA.

## Keyboard Shortcuts
|Key        |Function                            |
|-----------|------------------------------------|
|space      | capture frame                      |
|del        | delete last frame                  |
|f          | calculate and fix camera settings  |
|l          | toggle lights                      |
|p          | toggle preview                     |
|s          | create new sequence                |
|v          | update and play preview video      |
|z          | zip all frames                     |
|up/down    | adjust brightness                  |
|left/right | adjust contrast                    |
