#!/usr/bin/python3
#-----------------------------------------------------------
#    ___  ___  _ ____
#   / _ \/ _ \(_) __/__  __ __
#  / , _/ ___/ /\ \/ _ \/ // /
# /_/|_/_/  /_/___/ .__/\_, /
#                /_/   /___/
#
# Project : Stop Motion Animation Studio
# File    : interface.py
#
# Script to hold curses related functions that are called by camera.py
# Pulls screens from interface.dat.
#
# Author : Matt Hawkins
# Date   : 03/03/2019
# Source : https://bitbucket.org/MattHawkinsUK/rpispy-animation-studio/
#
# Additional details here:
# https://www.raspberrypi-spy.co.uk/
#
#-----------------------------------------------------------

# Standard libraries
import curses

# Project libraries
import config as c

# Variables for Screen 1
screen1={
        'cameraname':(3,15,10,''),
        'sequence'  :(4,15,4,''),
        'frame'     :(5,15,4,''),
        'brightness':(7,15,4,'50'),
        'contrast'  :(8,15,4,'0'),
        'lights'    :(11,10,1,' '),
        'overlay'   :(11,34,1,' '),
        'preview'   :(11,22,1,' ')
        }
# Variables for Screen 2
screen2={}
# Variables for Screen 3
screen3={}
# Variables for Screen 4
screen4={}

stdscr=""

currentInterface=1

# Functions
def screenMsg(y,x,msg):
  """Write message to screen."""
  stdscr.addstr(y,x,msg)

def stopInterface():
  """Stop curses interface and reset features before returning to the command line."""
  curses.curs_set(1)
  curses.nocbreak()
  stdscr.keypad(False)
  curses.echo()
  curses.endwin()

def drawInterface(templateNum=1):
  """Draw interface from template."""
  filepath = 'interface.dat'
  num=templateNum-1
  y=0
  with open(filepath) as fp:
    content = fp.readlines()
    for y in range(0,13):
      line = content[y+(num*13)]
      line = line.replace('\n', '').replace('\r', '')
      stdscr.addstr(y,0,line)
  currentInterface=templateNum
  displayAllParameters(templateNum)

def setupInterface():
  """Create curses interface."""
  global stdscr
  stdscr=curses.initscr()
  curses.curs_set(0)
  curses.noecho()
  curses.cbreak()
  stdscr.keypad(True)

def displayAllParameters(templateNum=1):
  """Display parameters currently stored in variables."""
  if templateNum==1:
    for parameter in screen1:
      (y,x,size,msg)=screen1[parameter]
      updateInterfaceValue(parameter,msg)

def updateInterfaceValue(parameter,value):
  """Update a specified parameter on the text interface with the specified value.
  The screen dictionary is used to obtain the y and x position. The size
  value determines how many characters this parameter uses.
  """

  (y,x,size,msg)=screen1[parameter]
    
  if parameter=="cameraname":
    msg=str(value)
    msg=msg[:size].ljust(size)
  elif parameter=="sequence":
    msg=str(value).zfill(size)
  elif parameter=="frame":
    msg=str(value).zfill(size)
  elif parameter=="brightness":
    msg=str(value).rjust(size)
  elif parameter=="contrast":
    msg=str(value).rjust(size)
  elif parameter=="lights":
    if value:
      msg="X"
    else:
      msg=" "
  elif parameter=="overlay":
    if value:
      msg="X"
    else:
      msg=" "      
  elif parameter=="preview":
    if value:
      msg="X"
    else:
      msg=" "

  screen1[parameter]=(y,x,size,msg)
  screenMsg(y,x,msg)
