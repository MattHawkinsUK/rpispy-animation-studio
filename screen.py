#!/usr/bin/python3
#-----------------------------------------------------------
#    ___  ___  _ ____
#   / _ \/ _ \(_) __/__  __ __
#  / , _/ ___/ /\ \/ _ \/ // /
# /_/|_/_/  /_/___/ .__/\_, /
#                /_/   /___/
#
# Project : Stop Motion Animation Studio
# File    : screen.py
#
# Script to provide functions for I2C screen.
#
# Author : Matt Hawkins
# Date   : 28/02/2019
# Source : https://bitbucket.org/MattHawkinsUK/rpispy-animation-studio/
#
# Additional details here:
# https://www.raspberrypi-spy.co.uk/
#
#-----------------------------------------------------------

# LCD screen libraries
import Adafruit_SSD1306
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

# Project libraries
import config as c

# Initialise OLED display
disp = Adafruit_SSD1306.SSD1306_128_32(rst=None)
disp.begin()
disp.clear()
disp.display()
image = Image.new('1', (c.LCDW, c.LCDH))
draw = ImageDraw.Draw(image)

# Load Truetype font from https://www.dafont.com/bitmap.php
# VCR OSD Mono by Riciery Leal
font = ImageFont.truetype('VCR_OSD_MONO_1.001.ttf',16)

def updateDisplay(seq,frame):
  """Update display with sequence and frame numbers."""
  draw.rectangle((0,0,c.LCDW,c.LCDH), outline=0, fill=0)
  # Write two lines of text.
  seqText=str(seq).zfill(4)
  fraText=str(frame).zfill(4)
  draw.text((0,-2),c.CAMNAME[:10],font=font,fill=255)
  draw.text((118,16),"T",font=font,fill=255)
  draw.text((0,16),seqText + " - " + fraText,font=font,fill=255)
  disp.image(image)
  disp.display()

def textDisplay(line1,line2=''):
  """Display text on screen."""
  # Display one or two lines of text
  draw.rectangle((0,0,c.LCDW,c.LCDH), outline=0, fill=0)
  draw.text((0, -2),line1,font=font,fill=255)
  draw.text((0, 16),line2,font=font,fill=255)
  disp.image(image)
  disp.display()
