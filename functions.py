#!/usr/bin/python3
#-----------------------------------------------------------
#    ___  ___  _ ____
#   / _ \/ _ \(_) __/__  __ __
#  / , _/ ___/ /\ \/ _ \/ // /
# /_/|_/_/  /_/___/ .__/\_, /
#                /_/   /___/
#
# Project : Stop Motion Animation Studio
# File    : functions.py
#
# Script to hold additional functions that are called by camera.py
#
# Author : Matt Hawkins
# Date   : 10/03/2019
# Source : https://bitbucket.org/MattHawkinsUK/rpispy-animation-studio/
#
# Additional details here:
# https://www.raspberrypi-spy.co.uk/
#
#-----------------------------------------------------------

# Standard libraries
import os
import math
import shutil
import subprocess
import zipfile
from PIL import Image

# Pi specific libraries
from gpiozero import LED, OutputDevice

# Project libraries
import config as c
import interface as i

# Variables
ledgrn = ""
ledyel = ""
light1 = ""
light2 = ""

# Functions
def getCurrentSeqNum():
  """Look at existing seq directories and find current sequence number.
  Assumes last 4 characters are sequence number
  """
  seq=1
  try:
    dirs=os.listdir(c.IMGDIR)
    for dir in dirs:
      if os.path.isdir(os.path.join(c.IMGDIR,dir)):
        seqChr=dir[-4:]
        seqNew=int(seqChr)
        if seqNew>=seq:
          seq=seqNew
  except:
    print("Error finding current sequence number!")

  return seq

def getNextSeqNum():
  """Look at existing seq directories and find next available sequence number.
  Assumes last 4 characters are sequence number.
  """
  seq=1
  try:
    seq=getCurrentSeqNum()
    seq=seq+1
  except:
    print("Error finding next seqeuence number!")

  return seq

def getNextFrameNum(seqNum):
  """Given a sequence number find next available frame number."""
  frame=1
  try:
    path=os.path.join(c.IMGDIR,getSeqName(seqNum))
    files=os.listdir(path)
    if files:
      files.sort(reverse=True)
      file=files[0]
      if file[0:6]=="frame_":
        frameChr=file[-8:-4]
        frameNew=int(frameChr)
        if frameNew>=frame:
          frame=frameNew+1
  except:
    print("Error determining next frame number!")

  return frame

def getFrameName(num,prefix=""):
  """Generate a frame file name from a frame number.
  Optional prefix can be specifed that will be placed at start of name.
  """
  frameName=prefix+'frame_'+str(num).zfill(4)+'.jpg'
  return frameName

def getSeqName(num):
  """Generate sequence folder name from sequence number."""
  seqName=c.PREFIX+'_seq'+str(num).zfill(4)
  return seqName

def getSeqPath(num):
  """Generate sequence path from a sequence number."""
  seqPath=os.path.join(c.IMGDIR,getSeqName(num))
  return seqPath

def createNewSeqDir():
  """Create new sequence directory."""
  seqNum=getNextSeqNum()
  createSeqDir(seqNum)
  return seqNum

def createSeqDir(seqNum):
  """Create sequence directory with specified sequence number."""
  dirName=getSeqName(seqNum)
  path=os.path.join(c.IMGDIR,dirName)
  try:
    # Create directory
    os.mkdir(path)
    # Set allow group to r+w
    os.chmod(path,0o775)
    
    # Copy default thumbnail from resources directory
    try:
      shutil.copy(os.path.join("resources","thumbnail.jpg"),os.path.join(path,"thumb_frame_0001.jpg"))
    except:
      pass
   
    i.updateInterfaceValue('sequence',seqNum)
  except OSError:
    print("Failed to create ", path)
  else:
    print("Created %s for sequence %04d" % (dirName,seqNum))

def deleteSeq(seq_dir):
  """Delete directory with specified sequence number."""
  currentPath = os.path.split(os.path.realpath(__file__))
  path=os.path.join(currentPath[0],c.IMGDIR,seq_dir)
  try:
    shutil.rmtree(path)
  except:
    print("Failed to delete ", path)
  else:
    print("Deleted ", path)

def deleteFrame(seq_dir,frame_name):
  """Delete specified frame and its thumbnail."""
  currentPath = os.path.split(os.path.realpath(__file__))
  path=os.path.join(currentPath[0],c.IMGDIR,seq_dir)
  if frame_name!="frame_0001.jpg":
    # Proceed if frame is not first one.
    frame_filename=os.path.join(path,frame_name)
    thumb_filename=os.path.join(path,"thumb_"+frame_name)
    try:
      os.remove(frame_filename)
      os.remove(thumb_filename)
    except:
      print("Failed to delete frame "+ frame_name + " from " + seq_dir)
    else:
      print("Deleted "+ frame_name + " from " + seq_dir)
  else:
      print("Skipping deletion of frame_0001.jpg from " + seq_dir)

def deleteFrameNum(seqNum,frameNum):
  """Delete specified frame and its thumbnail if number is greater than 1."""
  if frameNum>1:
    frameName=getFrameName(frameNum)
    seqName=getSeqName(seqNum)
    deleteFrame(seqName,frameName)

def createThumbnail(image,path):
  """Create a thumbnail of the specified image."""
  infile=os.path.join(path, image)
  outfile=os.path.join(path,'thumb_'+image)
  try:
    im = Image.open(infile)
    im.thumbnail(c.THUMBSIZE)
    im.save(outfile, "JPEG")
  except:
    print("  Failed to create thumb_", image)

def generatePreviewVideo(seq_dir=""):
  """Create preview video file using thumbnail images. If no sequence is given use latest."""
  if seq_dir=="":
    seq=getCurrentSeqNum()
    seq_dir=getSeqName(seq)
  path=os.path.join(c.IMGDIR,seq_dir)
  videoFile=os.path.join(path,'preview.mp4')
  frameFile=os.path.join(path,'thumb_frame_')
  subprocess.call(['ffmpeg', '-r', str(c.PREVIEWRATE), '-i', frameFile+'%04d.jpg','-qscale','2',videoFile])
  return videoFile

def generateArchiveFile(seq_dir=""):
  """Create zip fle of all frame images in specified sequence. If no sequence is given use latest."""
  if seq_dir=="":
    seq=getCurrentSeqNum()
    seq_dir=getSeqName(seq)
  seqpath=os.path.join(c.IMGDIR,seq_dir)
  zippath=os.path.join(seqpath,'archive.zip')

  zipobj = zipfile.ZipFile(zippath, 'w')

  frames=[]
  for entry in os.listdir(seqpath):
    if os.path.isfile(os.path.join(seqpath,entry)) == True:
      if entry[0:5]=='frame':
        zipobj.write(os.path.join(seqpath,entry), entry, compress_type = zipfile.ZIP_DEFLATED)

  zipobj.close()

  return zippath

def lights_toggle():
  """Toggle the state of the lights"""
  light1.toggle()
  light2.toggle()
  if light1.value==True:
    i.updateInterfaceValue('lights',True)
  else:
    i.updateInterfaceValue('lights',False)

def lights_state(state=False):
  """Turn the lights on or off."""
  if state==False:
    light1.off()
    light2.off()
    i.updateInterfaceValue('lights',False)
  elif state==True:
    light1.on()
    light2.on()
    i.updateInterfaceValue('lights',True)

def valueAdjust(value,mode='inc',step=5,min=0,max=100):
  """Increment or Decrement a value to the nearest step but stay within the min max boundary."""
  if mode=='dec':
    value=value-1
    newValue=math.floor(value/step)*step
  else:
    value=value+1
    newValue=math.ceil(value/step)*step
  if newValue>max: newValue=max
  if newValue<min: newValue=min
  return newValue

def shutdown():
  """Shutdown the system."""
  check_call('sudo','poweroff')

def setupGPIODevices():
  """Setup LEDs and relay attached to GPIO header."""
  global ledgrn
  global ledyel
  global light1
  global light2
  ledgrn = LED(c.GPIOLEDGRN)
  ledyel = LED(c.GPIOLEDYEL)
  light1 = OutputDevice(c.GPIOLIGHT1,active_high=False)
  light2 = OutputDevice(c.GPIOLIGHT2,active_high=False)
