#!/usr/bin/python3
#-----------------------------------------------------------
#    ___  ___  _ ____
#   / _ \/ _ \(_) __/__  __ __
#  / , _/ ___/ /\ \/ _ \/ // /
# /_/|_/_/  /_/___/ .__/\_, /
#                /_/   /___/
#
# Project : Stop Motion Animation Studio
# File    : camera.py
#
# This is the main script.
#
# Author : Matt Hawkins
# Date   : 01/03/2019
# Source : https://bitbucket.org/MattHawkinsUK/rpispy-animation-studio/
#
# Additional details here:
# https://www.raspberrypi-spy.co.uk/
#
#-----------------------------------------------------------

# Standard libraries
import os
import numpy as np
from time import sleep

# Pi specific libraries
from picamera import PiCamera
from gpiozero import Button

# LCD screen libraries
import Adafruit_SSD1306
from PIL import Image
from PIL import ImageDraw

# Project libraries
import config as c
import screen as s
import functions as f
import interface as i

# Functions
def capture():
  """Capture a frame."""
  f.ledyel.on()
  f.ledgrn.off()
  seqNum=f.getCurrentSeqNum()
  frameNum=f.getNextFrameNum(seqNum)
  filename=f.getFrameName(frameNum)
  path=os.path.join(c.IMGDIR,f.getSeqName(seqNum))
  fullpath=os.path.join(path,filename)
  camera.resolution = (c.PHOTOSIZE)
  camera.capture(fullpath)
  f.createThumbnail(filename,path)
  i.updateInterfaceValue('sequence',seqNum)  
  i.updateInterfaceValue('frame',frameNum)    
  s.updateDisplay(seqNum,frameNum)
  f.ledyel.off()
  f.ledgrn.on()

def cam_start_preview():
  """Start the camera preview."""
  # Start preview if it is not currently running
  if not camera.preview:
    camera.start_preview(resolution=c.PREVIEWSIZE)
  i.updateInterfaceValue('preview',True)   

def cam_stop_preview():
  """Stop the camera preview."""
  # Stop the preview if it is currently running
  cam_toggle_overlay("off")  
  camera.stop_preview()
  i.updateInterfaceValue('preview',False)

def cam_toggle_preview():
  """Toggle the state of the camera preview."""
  f.ledyel.blink(on_time=0.5, off_time=0.5, n=2)
  if camera.preview:
    cam_stop_preview()
  else:
    cam_start_preview()

def cam_create_overlay():
  """Create overlay."""
  img = Image.new('RGBA', (640,480),(0,0,0,0))
  draw = ImageDraw.Draw(img) 
  draw.line((213,30, 213,460), fill='#ffffff',width=2)
  draw.line((426,30, 426,460), fill='#ffffff',width=2)
  draw.line((0,160, 650,160), fill='#ffffff',width=2)
  draw.line((0,320, 650,320), fill='#ffffff',width=2)
  o=camera.add_overlay(img.tobytes(),size=img.size)
  o.layer=3
  
def cam_toggle_overlay(state=""):
  """Toggle the display of the overlay."""
  if state=="off":  
    for overlay in camera.overlays:
      camera.remove_overlay(overlay)
      i.updateInterfaceValue('overlay',False)
  elif not camera.overlays:
    cam_create_overlay()
    i.updateInterfaceValue('overlay',True)
  else:
    for overlay in camera.overlays:
      camera.remove_overlay(overlay)
    i.updateInterfaceValue('overlay',False)
  
def cam_reset():
  """Reset camera brightness and contrast to defaults."""
  camera.brightness=50
  camera.contrast=0
  i.updateInterfaceValue('brightness',50)
  i.updateInterfaceValue('contrast',0)
  
def fix_exposure():
  """Fix the camera exposure and restore current state of preview."""
  f.ledyel.on()
  f.ledgrn.off()
  s.textDisplay('Fix exposure','and AWB gain')
  preview=camera.preview
  cam_start_preview()
  camera.brightness=50
  camera.contrast=0
  camera.awb_mode = c.CAM_AWB_MODE
  camera.exposure_mode = 'auto'  
  sleep(5)
  c.CAM_EXPOSURE=camera.exposure_speed
  camera.shutter_speed = c.CAM_EXPOSURE
  camera.exposure_mode = 'off'
  c.CAM_AWB = camera.awb_gains
  camera.awb_mode = 'off'
  camera.awb_gains = c.CAM_AWB
  if not preview:
    cam_stop_preview()
  s.textDisplay('Fixed exposure','and AWB gain')  
  f.ledyel.off()
  f.ledgrn.on()

def graceful_quit():
  """Perform tasks before quitting."""
  camera.stop_preview()
  f.lights_state(False)
  i.stopInterface()

  print("Goodbye!")

# Setup hardware connected to the GPIO header."""
f.setupGPIODevices()

# Initialise text interface
i.setupInterface()
i.drawInterface()
i.updateInterfaceValue('cameraname',c.CAMNAME)
i.updateInterfaceValue('brightness',50)
i.updateInterfaceValue('contrast',0)

# Update I2C screen
s.textDisplay('RPiSpy Stop','Motion Studio')

# Create camera object
camera = PiCamera()
camera.rotation = c.CAMROTATION

# Set lights
f.lights_state(c.LIGHTS_INIT)

f.ledyel.on()

# Fix camera exposure and gain
fix_exposure()

# Create new sequence directory
seqNum=f.createNewSeqDir()

s.updateDisplay(seqNum,0)

f.ledgrn.on()
f.ledyel.off()

try:

  while True:

    # Look for keypresses
    chr = i.stdscr.getch()
    if chr == ord(' '):
      capture()
    elif chr == i.curses.KEY_DC:
      # Delete latest frame
      seqNum = f.getCurrentSeqNum()
      frameNum = f.getNextFrameNum(seqNum)-1
      f.deleteFrameNum(seqNum,frameNum)
      pass
    elif chr == ord('f'):
      # Fix exposure
      fix_exposure()
    elif chr == ord('b'):
      # TODO Play preview video
      pass
    elif chr == ord('l'):
      # Toggle lights
      f.lights_toggle()   
    elif chr == ord('p'):
      # Toggle preview
      cam_toggle_preview()
    elif chr == ord('o'):
      # Toggle overlay
      cam_toggle_overlay()      
    elif chr == ord('q'):
      # Quit
      break
    elif chr == ord('r'):
      # Reset camera settings
      cam_reset()   
    elif chr == ord('s'):
      # Create new sequence
      seqNum=f.createNewSeqDir()
      s.updateDisplay(seqNum,0)
    elif chr == ord('v'):
      # Generate preview video for current Sequence
      f.generatePreviewVideo()
    elif chr == ord('z'):
      # Generate archive file for current Sequence
      f.generateArchiveFile()      
    elif chr == i.curses.KEY_UP:
      camera.brightness=f.valueAdjust(camera.brightness)
      i.updateInterfaceValue('brightness',camera.brightness)
    elif chr == i.curses.KEY_DOWN:   
      camera.brightness=f.valueAdjust(camera.brightness,mode='dec')    
      i.updateInterfaceValue('brightness',camera.brightness)
    elif chr == i.curses.KEY_LEFT:
      camera.contrast=f.valueAdjust(camera.contrast,mode='dec',min=-100,max=100)
      i.updateInterfaceValue('contrast',camera.contrast)      
    elif chr == i.curses.KEY_RIGHT:
      camera.contrast=f.valueAdjust(camera.contrast,min=-100,max=100)
      i.updateInterfaceValue('contrast',camera.contrast)
    elif chr == i.curses.KEY_F1:
      i.drawInterface(1)
    elif chr == i.curses.KEY_F2:
      i.drawInterface(2)
    elif chr == i.curses.KEY_F3:
      i.drawInterface(3)
    elif chr == i.curses.KEY_F4:
      i.drawInterface(4)
    sleep(0.1)

  # Time to go!
  graceful_quit()

except:

  graceful_quit()
